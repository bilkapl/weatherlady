package pl.sdaacademy;

import pl.sdaacademy.openweatherapi.OpenWeatherApiService;
import pl.sdaacademy.weatherstack.WeatherStackService;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WeatherLadyService {

    private final WeatherLadyRepository weatherLadyRepository;

    public WeatherLadyService(WeatherLadyRepository weatherLadyRepository) {
        this.weatherLadyRepository = weatherLadyRepository;
    }

    public Weather fetchWeatherByCityName(String cityName) {
        OpenWeatherApiService openWeatherApiService = new OpenWeatherApiService();
        WeatherStackService weatherStackService = new WeatherStackService();
        List<Optional<Weather>> weathersOptionals = new ArrayList<>();
        weathersOptionals.add(openWeatherApiService.fetchByCityName(cityName));
        weathersOptionals.add(weatherStackService.fetchByCityName(cityName));
        List<Weather> weathers = weathersOptionals.stream()
                .filter(w -> w.isPresent())
                .map(w -> w.get())
                .collect(Collectors.toList());
        System.out.println(weathers);
        Weather weather = new Weather();
        weather.setCityName(cityName);
        weather.setTemp(getAverageTemperature(weathers));
        weather.setPressure(getAveragePressure(weathers));
        weather.setHumidity((int) getAverageHumidity(weathers));
        weather.setWindSpeed(getAverageWindSpeed(weathers));
        weather.setWindDeg((int) getAverageWindDeg(weathers));
        weather.setDateTime(getAverageDateTime(weathers));
        weatherLadyRepository.save(weather);
        return weather;
    }

    public Weather fetchWeatherByCoordinates(float lat, float lon) {
        OpenWeatherApiService openWeatherApiService = new OpenWeatherApiService();
        WeatherStackService weatherStackService = new WeatherStackService();
        List<Optional<Weather>> weathersOptionals = new ArrayList<>();
        weathersOptionals.add(openWeatherApiService.fetchByCoordinates(lat, lon));
        weathersOptionals.add(weatherStackService.fetchByCoordinates(lat, lon));
        List<Weather> weathers = weathersOptionals.stream()
                .filter(w -> w.isPresent())
                .map(w -> w.get())
                .collect(Collectors.toList());
        System.out.println(weathers);
        Weather weather = new Weather();
        weather.setCityName(weathers.stream()
                .findFirst()
                .map(w->w.getCityName()).orElse("unknown"));
        weather.setTemp(getAverageTemperature(weathers));
        weather.setPressure(getAveragePressure(weathers));
        weather.setHumidity((int) getAverageHumidity(weathers));
        weather.setWindSpeed(getAverageWindSpeed(weathers));
        weather.setWindDeg((int) getAverageWindDeg(weathers));
        weather.setDateTime(getAverageDateTime(weathers));
        weatherLadyRepository.save(weather);
        return weather;
    }

    private float getAverageTemperature(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> w.getTemp()).average().orElse(0.0);
    }

    private float getAveragePressure(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> w.getPressure()).average().orElse(0.0);
    }

    private float getAverageHumidity(List<Weather> weathers) {
        return (float) weathers.stream().mapToInt(w -> w.getHumidity()).average().orElse(0.0);
    }

    private float getAverageWindSpeed(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> w.getWindSpeed()).average().orElse(0.0);
    }

    private float getAverageWindDeg(List<Weather> weathers) {
        return (float) weathers.stream().mapToDouble(w -> w.getWindDeg()).average().orElse(0.0);
    }

    private LocalDateTime getAverageDateTime(List<Weather> weathers) {
        long averageDateTime = (long) weathers.stream().mapToDouble(w -> w.getDateTime().toEpochSecond(ZoneOffset.UTC)).average().orElse(0);
        return LocalDateTime.ofEpochSecond(averageDateTime, 0, ZoneOffset.UTC);
    }
}

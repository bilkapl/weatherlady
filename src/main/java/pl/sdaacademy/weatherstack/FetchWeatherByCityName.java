package pl.sdaacademy.weatherstack;

import pl.sdaacademy.BaseFetchWeatherByCityName;

class FetchWeatherByCityName extends BaseFetchWeatherByCityName<WeatherStack> {

    FetchWeatherByCityName(String cityName) {
        super(cityName);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCityNameQuery(cityName);
    }

    @Override
    public Class<WeatherStack> getClasz() {
        return WeatherStack.class;
    }
}

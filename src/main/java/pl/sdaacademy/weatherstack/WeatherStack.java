package pl.sdaacademy.weatherstack;

import com.google.gson.annotations.SerializedName;

class WeatherStack {
    private Location location;
    @SerializedName("current")
    private WeatherDetails weatherDetails;

    WeatherStack() {
    }

    WeatherStack(Location location, WeatherDetails weatherDetails) {
        this.location = location;
        this.weatherDetails = weatherDetails;
    }

    Location getLocation() {
        return location;
    }

    void setLocation(Location location) {
        this.location = location;
    }

    WeatherDetails getWeatherDetails() {
        return weatherDetails;
    }

    void setWeatherDetails(WeatherDetails weatherDetails) {
        this.weatherDetails = weatherDetails;
    }

    @Override
    public String toString() {
        return "WeatherStack{" +
                "location=" + location +
                ", weatherDetails=" + weatherDetails +
                '}';
    }
}

class WeatherDetails {
    private float temperature;
    private float pressure;
    private int humidity;
    @SerializedName("wind_speed")
    private float windSpeed;
    @SerializedName("wind_degree")
    private int windDegree;

    WeatherDetails() {
    }

    WeatherDetails(float temperature, float pressure, int humidity, float windSpeed, int windDegree) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegree = windDegree;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    float getPressure() {
        return pressure;
    }

    void setPressure(float pressure) {
        this.pressure = pressure;
    }

    int getHumidity() {
        return humidity;
    }

    void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    float getWindSpeed() {
        return windSpeed;
    }

    void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    int getWindDegree() {
        return windDegree;
    }

    void setWindDegree(int windDegree) {
        this.windDegree = windDegree;
    }

    @Override
    public String toString() {
        return "WeatherDetails{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                ", windDegree=" + windDegree +
                '}';
    }
}

class Location {
    private String name;
    @SerializedName("localtime_epoch")
    private long dateTime;


    Location() {
    }

    Location(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    long getDateTime() {
        return dateTime;
    }

    void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Location{" +
                "name='" + name + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }
}


/*
{
  "request": {
    "type": "City",
    "query": "Warszawa, Poland",
    "language": "en",
    "unit": "m"
  },
  "location": {
    "name": "Warszawa",
    "country": "Poland",
    "region": "",
    "lat": "52.250",
    "lon": "21.000",
    "timezone_id": "Europe/Warsaw",
    "localtime": "2022-04-24 12:29",
    "localtime_epoch": 1650803340,
    "utc_offset": "2.0"
  },
  "current": {
    "observation_time": "10:29 AM",
    "temperature": 12,
    "weather_code": 116,
    "weather_icons": [
      "https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
    ],
    "weather_descriptions": [
      "Partly cloudy"
    ],
    "wind_speed": 4,
    "wind_degree": 28,
    "wind_dir": "NNE",
    "pressure": 1007,
    "precip": 0,
    "humidity": 58,
    "cloudcover": 50,
    "feelslike": 12,
    "uv_index": 3,
    "visibility": 10,
    "is_day": "yes"
  }
}
 */
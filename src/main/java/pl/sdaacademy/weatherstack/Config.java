package pl.sdaacademy.weatherstack;

class Config {

    private static Config config;
    private final String baseAPIUrl;
    private final String appId;

    private Config(String baseAPIUrl, String appId) {
        this.baseAPIUrl = baseAPIUrl;
        this.appId = appId;
    }

    String getFetchByCityNameQuery(String cityName) {
        return String.format("%s?access_key=%s&query=%s", baseAPIUrl, appId, cityName);
    }

    String getFetchByCoordinatesQuery(float lat, float lon) {
        return String.format("%s?access_key=%s&query=%f,%f", baseAPIUrl, appId, lat, lon);
    }

    static Config getInstance() {
        if (config == null) {
            config = new Config("http://api.weatherstack.com/current", "606b8c7047ef08f93fe4e2278edfdc44");
        }
        return config;
    }
}

package pl.sdaacademy.weatherstack;

import pl.sdaacademy.BaseFetchWeatherByCoordinates;

class FetchWeatherByCoordinates extends BaseFetchWeatherByCoordinates<WeatherStack> {
    public FetchWeatherByCoordinates(float lat, float lon) {
        super(lat, lon);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCoordinatesQuery(lat, lon);
    }

    @Override
    public Class<WeatherStack> getClasz() {
        return WeatherStack.class;
    }
}

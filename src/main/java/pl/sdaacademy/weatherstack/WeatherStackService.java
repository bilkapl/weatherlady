package pl.sdaacademy.weatherstack;

import pl.sdaacademy.NetworkApiException;
import pl.sdaacademy.Weather;

import java.util.Optional;

public class WeatherStackService {

    public Optional<Weather> fetchByCityName(String cityName) {
        try {
            WeatherStack weatherStack = new FetchWeatherByCityName(cityName).execute();
            return Optional.of(WeatherTransformer.toWeather(weatherStack));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }

    public Optional<Weather> fetchByCoordinates(float lat, float lon) {
        try {
            WeatherStack weatherStack = new FetchWeatherByCoordinates(lat, lon).execute();
            return Optional.of(WeatherTransformer.toWeather(weatherStack));
        } catch (NetworkApiException e) {
            return Optional.empty();
        }
    }
}

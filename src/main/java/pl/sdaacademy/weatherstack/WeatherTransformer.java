package pl.sdaacademy.weatherstack;

import pl.sdaacademy.Weather;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.TimeZone;

class WeatherTransformer {

    static Weather toWeather(WeatherStack weatherStack) {
        Weather weather = new Weather();
        weather.setCityName(weatherStack.getLocation().getName());
        weather.setTemp(weatherStack.getWeatherDetails().getTemperature());
        weather.setHumidity(weatherStack.getWeatherDetails().getHumidity());
        weather.setPressure(weatherStack.getWeatherDetails().getPressure());
        weather.setWindSpeed(weatherStack.getWeatherDetails().getWindSpeed());
        weather.setWindDeg(weatherStack.getWeatherDetails().getWindDegree());
        weather.setDateTime(LocalDateTime.ofEpochSecond(weatherStack.getLocation().getDateTime(), 0, ZoneOffset.UTC));
        return weather;
    }
}

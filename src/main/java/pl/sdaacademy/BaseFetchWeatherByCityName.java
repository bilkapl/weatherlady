package pl.sdaacademy;

public abstract class BaseFetchWeatherByCityName<T> {

    protected final String cityName;

    public BaseFetchWeatherByCityName(String cityName) {
        this.cityName = cityName;
    }

    public final T execute() {
        return HttpClientWrapperProvider.getInstance().get(getUrl(), getClasz());
    }

    public abstract String getUrl();

    public abstract Class<T> getClasz();
}

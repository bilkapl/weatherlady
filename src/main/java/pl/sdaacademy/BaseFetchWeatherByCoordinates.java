package pl.sdaacademy;

public abstract class BaseFetchWeatherByCoordinates<T> {

    protected final float lat;
    protected final float lon;

    public BaseFetchWeatherByCoordinates(float lat, float lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public final T execute() {
        return HttpClientWrapperProvider.getInstance().get(getUrl(), getClasz());
    }

    public abstract String getUrl();

    public abstract Class<T> getClasz();
}

package pl.sdaacademy.openweatherapi;

import pl.sdaacademy.BaseFetchWeatherByCoordinates;

class FetchWeatherByCoordinates extends BaseFetchWeatherByCoordinates<OpenWeather> {

    public FetchWeatherByCoordinates(float lat, float lon) {
        super(lat, lon);
    }

    @Override
    public String getUrl() {
        return Config.getInstance().getFetchByCoordinatesQuery(lat, lon);
    }

    @Override
    public Class<OpenWeather> getClasz() {
        return OpenWeather.class;
    }
}
